import json
import urllib.request


class ApiEndpointsService:
    def __init__(self):
        self.free_bike_status_url = (
            "https://data-sharing.tier-services.io/tier_paris/gbfs/2.2/free-bike-status"
        )
        self.system_pricing_plans_url = "https://data-sharing.tier-services.io/tier_paris/gbfs/2.2/system-pricing-plans"

    def get_vehicles_data(self):
        vehicle_res = self.__api_endpoints_request(self.free_bike_status_url)
        vehicles = vehicle_res["data"]["bikes"]

        return vehicles

    def get_pricing_info(self):
        pricing_info_res = self.__api_endpoints_request(self.system_pricing_plans_url)
        prices = pricing_info_res["data"]["plans"]

        return prices

    def __api_endpoints_request(self, url: str):
        request = urllib.request.urlopen(url)
        response = json.loads(request.read().decode("utf-8"))

        return response
