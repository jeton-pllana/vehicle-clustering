import json
import logging
import os
from functools import wraps
from jsonschema import ValidationError
from vehicle_clustering import VehicleClustering

logging.basicConfig(format="%(asctime)s %(name)s %(levelname)s %(message)s")
logger = logging.getLogger("VehicleClustering")
logger.setLevel(os.environ.get("LOGGING", logging.DEBUG))


def log_event(function):
    @wraps(function)
    def wrapper(event, context):
        logger.info(f"\nIncoming event: '{event}'")
        result = function(event, context)
        logger.info(f"\nResult: '{result}'")
        return result

    return wrapper


@log_event
def clustering_vehicle_data_lambda_handler(event, context):
    try:
        response = VehicleClustering().handle_clustering_vehicle_data(event)
        result = {
            "vehicles": response,
            "total": len(response)
        }
        return {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Headers": "Content-Type,Authorization",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
                "Content-Type": "application/json",
            },
            "body": json.dumps(result, default=str),
        }
    except ValidationError as error:
        return {
            "statusCode": 400,
            "body": json.dumps(error.message, default=str),
        }


if __name__ == "__main__":
    base_dir = os.path.dirname(os.path.realpath(__file__))
    messages_dir = f"{base_dir}/resources"

    (_, _, filenames) = os.walk(messages_dir).__next__()
    filenames = [f"{messages_dir}/{filename}" for filename in filenames]

    for filename in filenames:
        with open(filename, encoding="utf8") as json_file:
            json_dict = json.load(json_file)
    clustering_vehicle_data_lambda_handler(json_dict, {})
