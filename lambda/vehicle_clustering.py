from typing import Dict
from geopy.geocoders import Nominatim
from services import ApiEndpointsService


class VehicleClustering:
    def __init__(self):
        self.api_endpoints_service = ApiEndpointsService()
        self.bikes = self.api_endpoints_service.get_vehicles_data()
        self.price_info = self.api_endpoints_service.get_pricing_info()

    def handle_clustering_vehicle_data(self, event: Dict):
        if event["httpMethod"] == "GET" and event["path"] == "/vehicles":
            vehicles = []
            for b in self.bikes:
                vehicle_aggr = {
                    "location": {"lat": b["lat"], "lon": b["lon"]},
                    "available": b["is_disabled"],
                    "price_info": self.get_price_information(b["pricing_plan_id"]),
                }
                vehicles.append(vehicle_aggr)

            if event["queryStringParameters"]:
                query_string_params = event["queryStringParameters"]
                available_param = query_string_params.get("available")
                location_param = query_string_params.get("location")

                if available_param:
                    available = True if available_param == "true" else False
                    vehicles = [
                        vehicle
                        for vehicle in vehicles
                        if vehicle["available"] == available
                    ]

                if location_param:
                    vehicles = self.filter_by_district_location(
                        location_param, vehicles
                    )

            return vehicles

    def get_price_information(self, pricing_plan_id: str):
        return next(
            (
                p["description"]
                for p in self.price_info
                if p["plan_id"] == pricing_plan_id
            ),
            None,
        )

    def filter_by_district_location(self, district: str, vehicles: []):
        geolocator = Nominatim(user_agent="VehicleClusteringApp")
        # since from the `free-bike-status` api almost all the vehicles are based in Paris,
        # we are only filtering by district within Paris.
        district_location = geolocator.geocode(f"{district}, Paris")
        district_lat = district_location.latitude
        district_lon = district_location.longitude

        vehicles_filtered_by_location = []
        # checking for vehicle(s) within approximately 3km - 3.5km radius
        # with regard to the district location
        for vehicle in vehicles:
            is_vehicle_in_district = (
                (float(vehicle["location"]["lat"]) <= district_lat + 0.02)
                & (float(vehicle["location"]["lat"]) >= district_lat - 0.02)
                & (float(vehicle["location"]["lon"]) <= district_lon + 0.027)
                & (float(vehicle["location"]["lon"]) >= district_lon - 0.027)
            )
            if is_vehicle_in_district:
                vehicles_filtered_by_location.append(vehicle)

        return vehicles_filtered_by_location
