import unittest
from unittest.mock import MagicMock, patch
from vehicle_clustering import VehicleClustering

fake_event = {
    "body": None,
    "headers": {},
    "httpMethod": "GET",
    "path": "/vehicles",
    "queryStringParameters": {"available": "false", "location": "Ternes"},
    "requestContext": {},
}
fake_vehicle_data = [
    {
        "bike_id": "111",
        "lat": 48.89508827761335,
        "lon": 2.321116180996501,
        "is_reserved": False,
        "is_disabled": False,
        "vehicle_type_id": "escooter_test",
        "current_range_meters": 32000,
        "pricing_plan_id": "100",
        "rental_uris": {
            "android": "test_url",
            "ios": "test_url",
        },
    },
    {
        "bike_id": "222",
        "lat": 48.851784,
        "lon": 2.367753,
        "is_reserved": False,
        "is_disabled": False,
        "vehicle_type_id": "escooter_test",
        "current_range_meters": 42000,
        "pricing_plan_id": "200",
        "rental_uris": {
            "android": "test_url",
            "ios": "test_url",
        },
    },
]
fake_aggregated_vehicle_data = [
    {
        "location": {"lat": 48.89508827761335, "lon": 2.321116180996501},
        "available": False,
        "price": "Standard pricing for scooters, 1.00 EUR to unlock, 0.25 EUR per minute to rent",
    },
    {
        "location": {"lat": 48.851784, "lon": 2.367753},
        "available": False,
        "price": "Standard pricing for scooters, 1.00 EUR to unlock, 0.25 EUR per minute to rent",
    }
]
fake_pricing_info = [
    {
        "plan_id": "100",
        "name": "test_plan+1",
        "currency": "EUR",
        "price": 1,
        "is_taxable": False,
        "description": "Standard pricing for scooters, 1.00 EUR to unlock, 0.25 EUR per minute to rent",
        "per_min_pricing": [{"start": 0, "rate": 0.25, "interval": 1}],
    },
    {
        "plan_id": "200",
        "name": "test_plan_2",
        "currency": "EUR",
        "price": 1,
        "is_taxable": False,
        "description": "Standard pricing for bikes, 1.00 EUR to unlock, 0.25 EUR per minute to rent",
        "per_min_pricing": [{"start": 0, "rate": 0.25, "interval": 1}],
    },
]


class VehicleClusteringTest(unittest.TestCase):
    def setUp(self) -> None:
        self.vehicle_clustering = VehicleClustering()

    def test_handler(self):
        self.vehicle_clustering.api_endpoints_service.get_vehicles_data = MagicMock(
            return_value=[]
        )
        self.vehicle_clustering.api_endpoints_service.get_pricing_info = MagicMock(
            return_value=[]
        )
        self.vehicle_clustering.bikes = fake_vehicle_data
        self.vehicle_clustering.price_info = fake_pricing_info
        res = self.vehicle_clustering.handle_clustering_vehicle_data(fake_event)
        self.assertEqual(len(res), 1)

    def test_get_price_information(self):
        self.vehicle_clustering.price_info = fake_pricing_info
        res = self.vehicle_clustering.get_price_information("100")
        information = "Standard pricing for scooters, 1.00 EUR to unlock, 0.25 EUR per minute to rent"
        self.assertEqual(res, information)

    def test_filter_by_district_location(self):
        res = self.vehicle_clustering.filter_by_district_location("Ternes", fake_aggregated_vehicle_data)
        self.assertEqual(len(res), 1)

