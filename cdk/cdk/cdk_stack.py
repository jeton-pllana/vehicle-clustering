from aws_cdk import (
    aws_ec2 as ec2,
    aws_ssm as ssm,
    aws_lambda as lambda_,
    aws_iam as iam,
    core as cdk,
    aws_apigatewayv2 as apigateway,
)


class VehicleClusteringStack(cdk.Stack):
    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        global_env_name = self.node.try_get_context("globalEnvName")
        if global_env_name is None:
            raise EnvironmentError("global name is not defined")

        # assuming that we have a VPC created
        vpc_id = ssm.StringParameter.value_for_string_parameter(
            self, f"{global_env_name}-vpcId"
        )

        # assuming that we have an API Gateway created
        api_gw_id = ssm.StringParameter.value_for_string_parameter(
            self, f"{global_env_name}-apiGwId"
        )

        availability_zones = cdk.Fn.split(
            ",",
            ssm.StringParameter.value_for_string_parameter(
                self, f"{global_env_name}-availabilityZones"
            ),
        )

        private_subnet_ids = cdk.Fn.split(
            ",",
            ssm.StringParameter.value_for_string_parameter(
                self, f"{global_env_name}-privateSubnetIds"
            ),
        )

        vpc = ec2.Vpc.from_vpc_attributes(
            self,
            "Vpc",
            vpc_id=vpc_id,
            availability_zones=availability_zones,
            private_subnet_ids=private_subnet_ids,
        )

        service_principal = iam.ServicePrincipal("apigateway.amazonaws.com")

        source_arn = self.format_arn(
            service="execute-api", resource=api_gw_id, sep="/", resource_name="*"
        )

        # Vehicle Clustering Lambda
        clustering_vehicle_data_lambda = lambda_.DockerImageFunction(
            self,
            "VehicleClusteringLambda",
            code=lambda_.DockerImageCode.from_image_asset("../lambda"),
            vpc=vpc,
            timeout=cdk.Duration.seconds(300),
            memory_size=512,
            environment={
                "ENV": "cloud",
            },
        )

        clustering_vehicle_data_lambda_integration = apigateway.CfnIntegration(
            self,
            "clusteringVehicleDataLambdaIntegration",
            api_id=api_gw_id,
            payload_format_version="2.0",
            integration_type="AWS_PROXY",
            integration_uri=clustering_vehicle_data_lambda.function_arn,
        )

        apigateway.CfnRoute(
            self,
            "clusteringVehicleDataLambdaRoute",
            api_id=api_gw_id,
            authorization_type="AWS_IAM",
            route_key="GET /vehicles",
            target=cdk.Fn.join("", ["integrations/", clustering_vehicle_data_lambda_integration.ref]),
        )

        clustering_vehicle_data_lambda.add_permission(
            "apiGtwLambdaPerm", principal=service_principal, source_arn=source_arn
        )
        clustering_vehicle_data_lambda.role.add_managed_policy(
            iam.ManagedPolicy.from_aws_managed_policy_name("SecretsManagerReadWrite")
        )
