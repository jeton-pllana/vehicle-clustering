#!/usr/bin/env python3
import os
from aws_cdk import core as cdk
from cdk.cdk_stack import VehicleClusteringStack


app = cdk.App()
global_env_name = app.node.try_get_context("globalEnvName")
if global_env_name is None:
    raise EnvironmentError("globalEnvName is not set in context")

VehicleClusteringStack(
    app,
    f"{global_env_name}-VehicleClusteringStack",
    env=cdk.Environment(
        account=os.getenv("CDK_DEFAULT_ACCOUNT"), region=os.getenv("CDK_DEFAULT_REGION")
    ),
)

app.synth()
