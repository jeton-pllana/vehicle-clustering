# Vehicle Clustering Data

This project is capable of clustering the vehicle data (location, availability and pricing). This project will provide the data via an API `/vehicles`.
This endpoint also provides filtering based on location and availability.
For filtering just add `location` and `availability` as a query string parameters. for example: `/vehicles?available=true&location=Ternes`.
Based on the public available API endpoints for getting the vehicle data, it seems that all vehicles are based in Paris, and for this reason the filtering is implemented by filtering based on Paris districts. For example if the location is `Ternes` it will show only vehicles in that district. The way that is filtered is by getting the vehicles in approximately 3km radius based on specified district. Here is the list of Paris districts: https://fr.wikipedia.org/wiki/Liste_des_quartiers_administratifs_de_Paris

NOTE: In the project it is also implemented to create and also deploy the project using the AWS CDK. Since I didn't have a private AWS Account I couldn't test it, but someone having AWS account and maybe with some small adjustments can deploy it with this command: `cdk deploy --require-approval never --all --context globalEnvName=${GLOBAL_ENV_NAME}`. For this you need to connect to your AWS Account from your terminal, and then specify the environment you want to deploy by replacing `GLOBAL_ENV_NAME` with you environment name. With this command, it will generate the Lambda function and integrate with the API Gateway.
Also, there is `.gitlab-ci.yml` file where is used to deploy it through CI, but it needs some adjustments also for the AWS Accounts, so for now I just added some placeholders.

## Api documentation
You can find a OAS on https://gitlab.com/jeton-pllana/vehicle-clustering/-/blob/main/documentation/openapi.json

## Getting started

Steps for Local development. We can run this project locally in two ways:

### Running through terminal by running `main.py` file.
1. If this is the first time you are running this project, you need to create a virtual environment for installing its dependencies. You do this by running `python -m venv venv` in the root directory.
2. Activate the project's virtual environment by running the following script: Linux `./venv/bin/activate`, for Windows `venv/Scripts/actiave`
3. Install all the needed dependencies, go to lambda folder and run  `pip install -r requirements.txt`.
4. Run main python file `python main.py` in lambda folder to test the lambda locally
5. You can use/modify the event that we need to send to lambda at: `lambda/resources/search_event.json`

### Running using AWS SAM
1. AWS CLI and AWS SAM CLI are required to be installed in your machine.
2. Using your preferred terminal, move to the root folder where the `template.yaml` file is located.
3. Run `sam build`
4. After it builds successfully run this command: `sam local start-api`
5. The project will be running and you can access through `http://127.0.0.1:3000`
6. By using your browser or any API platform e.g. Postman, you can execute the api `http://127.0.0.1:3000/vehicles`


## Unit test

For unit tests we use the python package unittest.
If you want to add more unit tests navigate to `./lambda/tests` and add a new test class.
In order to run the unit tests execute
1. Navigate to `./lambda`
2. Execute:
```shell
$ unittest tests/*.py
```


### Coverage

If you want to get information about the code coverage you can use the coverage package.
You get the coverage information by running:

```shell
$ coverage run --branch -m unittest tests/*.py
```

To get the report, execute:

```shell
$ coverage report
```

This prints the coverage information to the terminal.

To get a html report where one can navigate and inspect uncoveraded lines execute:

```shell
$ coverage html
```